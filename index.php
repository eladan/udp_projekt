<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>PROJEKT AZURE | Przemysław Potomski  </title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body >
        <!-- Navigation-->



        <!-- Portfolio Section-->
<div style="margin-top: 100px" class="col-lg-12">
    <button onclick="$('#dodajprodukt').modal('show')" class="btn btn-success">Dodaj</button>
    <table class="table">
        <thead >
        <th>ID</th>
        <th>Produkt</th>
        <th>Obraz</th>
        </thead>
        <tbody>
        <?php

        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);

        error_reporting(E_ALL);
        require_once "funkcje/baza.php";
            dbInit();
        $conn = NowePolaczenie();
        $query = mysqli_query($conn,"Select * from products");


       while($row=mysqli_fetch_array($query)){
       echo "  <tr>
            <td>".$row['ID']."</td>
            <td>".$row['NAME']."</td>
            <td><img width='100px' height='100px;' src='".$row['IMG']."'></td>
   
        </tr>";
        } ?>

        </tbody>
    </table>

</div>
        <!-- About Section-->
        <div id="dodajprodukt"   data-keyboard="false" data-backdrop="static" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="dodajprodukt"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content" id="modaldodajprodukt">
                    <div style="padding-bottom: 20px" class="modal-body">
                        <div class="col-lg-12">

                            <form method="post" enctype="multipart/form-data" action="funkcje/nowyprodukt.php">

                                <input class="form-control" type="text" name="filename">
                                <input class="form-control" type="file" name="fileToUpload">
                                <button type="submit" class="btn btn-success">Dodaj</button>



                            </form>
                        </div>


                    </div>
                </div>
            </div>

        </div>
        </div>

        <!-- Footer-->
        <footer class="footer text-center">
            <div class="container">
                <div class="row">
                    <!-- Footer Location-->

                    <!-- Footer Social Icons-->

                    <!-- Footer About Text-->

                </div>
            </div>
        </footer>
        <!-- Copyright Section-->
        <div class="copyright py-4 text-center text-white">
            <div class="container"><small>Przemysław Potomski</small></div>
        </div>
        <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes)-->
        <div class="scroll-to-top d-lg-none position-fixed">
            <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top"><i class="fa fa-chevron-up"></i></a>
        </div>
        <!-- Portfolio Modals-->
        <!-- Portfolio Modal 1-->


        <!-- Bootstrap core JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <!-- Contact form JS-->
        <script src="assets/mail/jqBootstrapValidation.js"></script>
        <script src="assets/mail/contact_me.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
